# Baseball Dynasties Stats

A simple Clojurescript browser application for calculating the SD Score
statistic. Inspired by the SD Score statistic in the book _Baseball Dynasties_.

## Development

### Running the App

Start a temporary local web server, build the app with the `dev` profile, and
serve the app, browser test runner and karma test runner with hot reload:

```sh
npm install
npx shadow-cljs watch app
```

Please be patient; it may take over 20 seconds to see any output, and over 40
seconds to complete.

When `[:app] Build completed` appears in the output, browse to
[http://localhost:8280/](http://localhost:8280/).

## Production

Build the app with the `prod` profile:

```sh
npm install
npm run release
```

Please be patient; it may take over 15 seconds to see any output, and over 30
seconds to complete.

The `resources/public/js/compiled` directory is created, containing the
compiled `app.js` and
`manifest.edn` files.
