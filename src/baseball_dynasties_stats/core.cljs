(ns baseball-dynasties-stats.core
  (:require
   [reagent.dom]
   [re-frame.core :as re-frame]
   [baseball-dynasties-stats.events :as events]
   [baseball-dynasties-stats.routes :as routes]
   [baseball-dynasties-stats.views :as views]
   [baseball-dynasties-stats.config :as config]))

(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (reagent.dom/unmount-component-at-node root-el)
    (reagent.dom/render [views/main-panel] root-el)))

(defn init []
  (routes/app-routes)
  (re-frame/dispatch-sync [::events/initialize-db])
  (re-frame/dispatch-sync [::events/fetch-franchises])
  (dev-setup)
  (mount-root))
