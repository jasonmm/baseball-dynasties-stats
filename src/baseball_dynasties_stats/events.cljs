(ns baseball-dynasties-stats.events
  (:require
    [re-frame.core :refer [reg-event-db reg-event-fx]]
    [ajax.core :as ajax]
    [day8.re-frame.http-fx]
    [day8.re-frame.async-flow-fx :as async-flow-fx]
    [baseball-dynasties-stats.db :as db]
    [clojure.string :as str]))

(defn target-value
  "A helper function for getting the value of a form field from an event, `e`."
  [e]
  (-> e .-target .-value))

(defn year-span
  "Returns a string of the years the given franchise was active."
  [f]
  (if (= (:firstYear f) (:lastYear f))
    (:firstYear f)
    (str (:firstYear f) "-" (:lastYear f))))

(defn year-range->vector
  [range-str]
  (let [[s e] (map js/parseInt (str/split range-str #"-"))]
    (range s (inc e))))

(defn years-str->vector
  [years-str]
  (flatten (map (fn [g]
                  (if (not= -1 (.indexOf g "-"))
                    (year-range->vector g)
                    (js/parseInt g)))
                (str/split years-str #","))))

(defn parse-json-for-app-db
  "Converts the JSON data retrieved from the data source into a map to be stored
  in the app-db."
  [json-data]
  (let [ks   (map keyword (get json-data "columns"))
        rows (get json-data "rows")]
    (map #(zipmap ks %) rows)))

(defn in-coll
  "Whether `item` is in `coll`."
  [coll item]
  (< -1 (.indexOf coll item)))

(defn in-years
  "Whether the data-item's year is in the vector `years`."
  [years data-item]
  (in-coll years (:yearID data-item)))

(defn franchise-for-years
  [franchise-id years data]
  (filter #(and (= franchise-id (:franchID %))
                (in-years years %))
          data))

(defn data-items-runs-for-years
  "Given a collection of data items return the sum of the runs keys."
  [data-items]
  (->> data-items
       (map #(select-keys % [:R :RA :ER]))
       (reduce (fn [carry item] (merge-with + carry item)))))

(defn franchise-runs-for-years
  [franchise-id years data]
  (->> (franchise-for-years franchise-id years data)
       data-items-runs-for-years))

(defn league-runs-for-years
  [years data]
  (->> data
       (filter (partial in-years years))
       data-items-runs-for-years))

(comment
  (def data @(re-frame.core/subscribe [:baseball-dynasties-stats.subs/data]))
  (league-runs-for-years [1903] data)
  (franchise-for-years "BOS" [1903] data)
  (distinct (map :yearID data))
  #_())

(reg-event-db
  ::initialize-db
  (fn [_ _]
    db/default-db))

(reg-event-fx
  ::navigate
  (fn [_ [_ handler]]
    {:navigate handler}))

(reg-event-fx
  ::set-active-panel
  (fn [{:keys [db]} [_ active-panel]]
    {:db (assoc db :active-panel active-panel)}))

(reg-event-db
  ::fetch-franchises-success
  (fn [db [_ response]]
    (-> db
        (assoc :franchises (parse-json-for-app-db response))
        (assoc-in [:requests :fetch-franchises] false))))

(reg-event-db
  ::fetch-franchises-failure
  (fn [db _]
    (-> db
        (assoc :fetch-franchises-failure true)
        (assoc-in [:requests :fetch-franchises] false))))

(reg-event-fx
  ; Make an HTTP request to retrieve all the possible franchises and the first
  ; and last years they were active.
  ::fetch-franchises
  (fn [{:keys [db]} _]
    {:db         (assoc-in db [:requests :fetch-franchises] true)
     :http-xhrio {:method          :get
                  :uri             "https://baseballdb.lawlesst.net/chadwick.json?sql=select%0D%0A++t.franchID%2C%0D%0A++f.franchName%2C%0D%0A++min(t.yearID)+as+firstYear%2C%0D%0A++max(t.yearID)+as+lastYear%0D%0Afrom%0D%0A++Teams+t%0D%0A++inner+join+TeamsFranchises+f+on+t.franchID+%3D+f.franchID%0D%0Agroup+by+t.franchID"
                  :response-format (ajax/json-response-format)
                  :on-success      [::fetch-franchises-success]
                  :on-failure      [::fetch-franchises-failure]}}))

(reg-event-fx
  ; A no-op event that is dispatched to notify the async-flow that the franchise
  ; runs are in the app-db.
  ::franchise-runs-fetched
  (fn [_ _]
    {}))

(reg-event-fx
  ::fetch-franchise-runs-success
  (fn [{:keys [db]} [_ response]]
    {:db (-> db
             (assoc :franchise-runs (first (parse-json-for-app-db response)))
             (assoc-in [:requests :fetch-franchise-runs] false))
     :fx [[:dispatch [::franchise-runs-fetched]]]}))

(reg-event-db
  ::fetch-franchise-runs-failure
  (fn [db _]
    (-> db
        (assoc :fetch-franchise-runs-failure true)
        (assoc-in [:requests :fetch-franchise-runs] false))))

(reg-event-fx
  ; Make an HTTP request to retrieve the currently selected franchise's run
  ; totals for the currently selected years.
  ::fetch-franchise-runs
  (fn [{:keys [db]} _]
    (let [franchise-id (:franchise-id db)
          years        (:years db)
          uri          (str "https://baseballdb.lawlesst.net/chadwick.json?sql=select%0D%0A++t.franchID%2C%0D%0A++sum(t.R)+as+R%2C%0D%0A++sum(t.RA)+as+RA%2C%0D%0A++sum(t.ER)+as+ER%0D%0Afrom%0D%0A++Teams+t%0D%0Awhere+t.franchID%3D%22" franchise-id "%22+and+t.yearID+in+(" (str/join "," years) ")%0D%0Agroup+by+t.franchID")]
      (if (and (coll? years)
               (not= uri (get-in db [:last-uri :fetch-franchise-runs])))
        {:db         (-> db
                         (assoc-in [:requests :fetch-franchise-runs] true)
                         (assoc-in [:last-uri :fetch-franchise-runs] uri))
         :http-xhrio {:method          :get
                      :uri             uri
                      :response-format (ajax/json-response-format)
                      :on-success      [::fetch-franchise-runs-success]
                      :on-failure      [::fetch-franchise-runs-failure]}}
        {:fx [[:dispatch [::franchise-runs-fetched]]]}))))

(reg-event-fx
  ; A no-op event that is dispatched to notify the async-flow that the league
  ; runs are in the app-db.
  ::league-runs-fetched
  (fn [_ _]
    {}))

(reg-event-fx
  ::fetch-league-runs-success
  (fn [{:keys [db]} [_ response]]
    {:db (-> db
             (assoc :league-runs (parse-json-for-app-db response))
             (assoc-in [:requests :fetch-league-runs] false))
     :fx [[:dispatch [::league-runs-fetched]]]}))

(reg-event-db
  ::fetch-league-runs-failure
  (fn [db _]
    (-> db
        (assoc :fetch-league-runs-failure true)
        (assoc-in [:requests :fetch-league-runs] false))))

(reg-event-fx
  ::fetch-league-runs
  (fn [{:keys [db]} _]
    (let [years (:years db)
          uri   (str "https://baseballdb.lawlesst.net/chadwick.json?sql=select%0D%0A++t.franchID%2C%0D%0A++count%28t.yearID%29+as+numYears%2C%0D%0A++sum%28t.R%29+as+R%2C%0D%0A++sum%28t.RA%29+as+RA%2C%0D%0A++sum%28t.ER%29+as+ER%0D%0Afrom%0D%0A++Teams+t%0D%0Awhere%0D%0A++t.yearID+in+%28" (str/join "," years) "%29%0D%0Agroup+by%0D%0A++t.franchID")]
      (if (and (coll? years)
               (not= uri (get-in db [:last-uri :fetch-league-runs])))
        {:db         (-> db
                         (assoc-in [:requests :fetch-league-runs] true)
                         (assoc-in [:last-uri :fetch-league-runs] uri))
         :http-xhrio {:method          :get
                      :uri             uri
                      :response-format (ajax/json-response-format)
                      :on-success      [::fetch-league-runs-success]
                      :on-failure      [::fetch-league-runs-failure]}}
        {:fx [[:dispatch [::league-runs-fetched]]]}))))

(reg-event-db
  ::parse-and-set-years
  (fn [db [_ years-str]]
    (assoc db :years (years-str->vector years-str)
              :years-input years-str)))

(reg-event-db
  ::set-franchise
  (fn [db [_ franchise]]
    (assoc db :franchise-id franchise)))

(defn franchise-avg-runs-per-year
  "Given league data returned by the request in
  `:baseball-dynasties-stats.events/fetch-league-runs` return a collection of
  maps containing the average runs score and runs allowed by each franchise in
  a year."
  [league-runs]
  (map (fn [f]
         {:franchID (:franchID f)
          :R        (/ (:R f) (:numYears f))
          :RA       (/ (:RA f) (:numYears f))})
       league-runs))

(defn league-mean
  "Return a map with the mean of both the runs scored and runs allowed given the
  league data returned by the request in
  `:baseball-dynasties-stats.events/fetch-league-runs`."
  [league-runs]
  (let [num-franchises (count league-runs)
        runs-per-year  (franchise-avg-runs-per-year league-runs)
        r-sum          (reduce + 0 (map :R runs-per-year))
        ra-sum         (reduce + 0 (map :RA runs-per-year))]
    {:R              (quot r-sum num-franchises)
     :RA             (quot ra-sum num-franchises)
     :runs-per-year  runs-per-year
     :num-franchises num-franchises}))

(defn standard-deviation
  "https://github.com/clojure-cookbook/clojure-cookbook/blob/master/01_primitive-data/1-20_simple-statistics.asciidoc"
  [coll mean]
  (let [squares (for [x coll]
                  (let [x-avg (- x mean)]
                    (* x-avg x-avg)))
        total   (count coll)]
    (-> (/ (apply + squares)
           (- total 1))
        (Math/sqrt))))

(defn league-sd
  "Return a map with the standard deviation of both the runs scored and runs
  allowed given the league data returned by the request in
  `:baseball-dynasties-stats.events/fetch-league-runs`."
  [league-runs league-mean]
  (let [runs-per-year (franchise-avg-runs-per-year league-runs)
        r-sd          (standard-deviation (map :R runs-per-year) (:R league-mean))
        ra-sd         (standard-deviation (map :RA runs-per-year) (:RA league-mean))]
    {:R  r-sd
     :RA ra-sd}))

(reg-event-db
  ::calculate-sd-score
  (fn [db _]
    (let [league-mean    (league-mean (:league-runs db))
          league-sd      (league-sd (:league-runs db) league-mean)
          franchise-runs {:R  (/ (get-in db [:franchise-runs :R])
                                 (count (:years db)))
                          :RA (/ (get-in db [:franchise-runs :RA])
                                 (count (:years db)))}
          r-sd-score     (/ (- (:R franchise-runs)
                               (:R league-mean))
                            (:R league-sd))
          ra-sd-score    (/ (- (:RA franchise-runs)
                               (:RA league-mean))
                            (:RA league-sd))]
      (assoc db :r-sd-score r-sd-score
                :ra-sd-score ra-sd-score
                :league-mean league-mean
                :league-sd league-sd))))

(reg-event-db
  ::sd-score-calculation-start
  (fn [db _]
    (assoc db :sd-calculation-in-progress? true)))

(reg-event-db
  ::sd-score-calculation-end
  (fn [db _]
    (assoc db :sd-calculation-in-progress? false)))

(defn calculate-sd-score-flow
  []
  {:first-dispatch [::sd-score-calculation-start]
   :rules          [{:when     :seen?
                     :events   ::sd-score-calculation-start
                     :dispatch [::fetch-franchise-runs]}
                    {:when     :seen?
                     :events   ::franchise-runs-fetched
                     :dispatch [::fetch-league-runs]}
                    {:when       :seen?
                     :events     ::league-runs-fetched
                     :dispatch-n [[::calculate-sd-score] [::sd-score-calculation-end]]
                     :halt?      true}
                    {:when     :seen-any-of?
                     :events   [::fetch-franchise-runs-failure ::fetch-league-runs-failure]
                     :dispatch [::sd-score-calculation-end]
                     :halt?    true}]})

(reg-event-fx
  ::calculate-sd-score-button-click
  (fn [_ _]
    {:async-flow (calculate-sd-score-flow)}))
