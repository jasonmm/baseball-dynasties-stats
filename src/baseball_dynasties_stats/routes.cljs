(ns baseball-dynasties-stats.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import goog.History)
  (:require
    [goog.events :as gevents]
    [goog.history.EventType :as EventType]
    [re-frame.core :as re-frame]
    [secretary.core :as secretary]
    [baseball-dynasties-stats.events :as events]))

(defmulti panels identity)
(defmethod panels :default [panel-kw] [:div "No panel found for this route: " panel-kw])

(defn hook-browser-navigation! []
  (doto (History.)
    (gevents/listen
      EventType/NAVIGATE
      (fn [event]
        (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes
  []
  (secretary/set-config! :prefix "#")

  (defroute "/" [] (re-frame/dispatch [::events/set-active-panel :home-panel]))
  (defroute "/about" [] (re-frame/dispatch [::events/set-active-panel :about-panel]))

  (hook-browser-navigation!))
