(ns baseball-dynasties-stats.views
  (:require
    [re-frame.core :as re]
    [baseball-dynasties-stats.events :as events]
    [baseball-dynasties-stats.routes :as routes]
    [baseball-dynasties-stats.subs :as subs :refer [<sub]]))

(defn franchise-select
  []
  (let [franchises (<sub [::subs/franchises])]
    [:<>
     [:label {:for "franchise-selector"} "Franchise"]
     [:select {:id        "franchise-selector"
               :disabled  (<sub [::subs/request-in-progress? :fetch-franchises])
               :value     (<sub [::subs/selected-franchise])
               :on-change #(re/dispatch [::events/set-franchise (events/target-value %)])}
      [:option {:key "" :value "" :disabled true} ""]
      (for [f franchises]
        [:option
         {:key (:franchID f) :value (:franchID f)}
         (str (:franchID f) " " (:franchName f) " (" (events/year-span f) ")")])]]))

(defn years-input
  []
  (let [years-input        (<sub [::subs/years-input])
        selected-franchise (<sub [::subs/selected-franchise])]
    [:<>
     [:label {:for "years"} "Years"]
     [:input {:type        :text
              :id          "years"
              :disabled    (= 0 (count selected-franchise))
              :placeholder "Years (e.g. 1982,1985-1989,1990)"
              :value       years-input
              :on-change   #(re/dispatch [::events/parse-and-set-years (events/target-value %)])}]]))

(defn sd-score
  []
  (let [sd-score       (<sub [::subs/sd-score-fixed-precision])
        league-mean    (<sub [::subs/league-mean])
        league-sd      (<sub [::subs/league-sd])
        franchise-runs (<sub [::subs/franchise-runs])
        r-sd-score     (<sub [::subs/runs-scored-sd-score-fixed-precision])
        ra-sd-score    (<sub [::subs/runs-allowed-sd-score-fixed-precision])]
    (when (and (not (<sub [::subs/sd-score-calculation-in-progress?]))
               sd-score)
      [:<>
       [:div.row
        [:div.column.text-center
         [:div.row [:div.column.text-center "League Mean Runs"]]
         [:div.row [:div.column.text-center league-mean]]]
        [:div.column.text-center
         [:div.row [:div.column.text-center "League Runs SD"]]
         [:div.row
          [:div.column.text-center
           [:div.row [:div.column.text-center (str "R: " (:R league-sd))]]
           [:div.row [:div.column.text-center (str "RA: " (:RA league-sd))]]]]]
        [:div.column.text-center
         [:div.row [:div.column.text-center "Franchise Runs"]]
         [:div.row
          [:div.column.text-center
           [:div.row [:div.column.text-center (str "R: " (:R franchise-runs))]]
           [:div.row [:div.column.text-center (str "RA: " (:RA franchise-runs))]]]]]
        [:div.column.text-center
         [:div.row [:div.column.text-center "Runs Scored SD Score"]]
         [:div.row [:div.column.text-center r-sd-score]]]
        [:div.column.text-center
         [:div.row [:div.column.text-center "Runs Allowed SD Score"]]
         [:div.row [:div.column.text-center ra-sd-score]]]]

       [:br] [:br]

       [:div.row
        [:div.column.text-center [:strong "SD Score"]]]
       [:div.row
        [:div.column.text-center [:strong sd-score]]]])))

;; home

(defn home-panel
  []
  [:div.container
   [:div.row
    [:div.column [:h3 "Baseball Dynasties SD Score Calculator"]]
    [:div.column {:style {:text-align :right}} [:a {:href "#/about"} "About"]]]
   [:div.row
    [:div.column [franchise-select]]
    [:div.column [years-input]]
    [:div.column.text-center
     [:br]
     [:button.button
      {:on-click #(re/dispatch [::events/calculate-sd-score-button-click])
       :disabled (<sub [::subs/request-in-progress? :fetch-franchise-runs])}
      "Calculate SD Score"]]]
   [:div.row
    [:div.column [sd-score]]]])

(defmethod routes/panels :home-panel [] [home-panel])

;; about

(defn about-panel []
  [:div.container
   [:div.row
    [:div.column [:h3 "Baseball Dynasties SD Score Calculator"]]
    [:div.column {:style {:text-align :right}} [:a {:href "#/"} "Home"]]]

   [:p "Calculate an SD Score for teams. Inspired by the SD Score statistic in book " [:em "Baseball Dynasties"] "."]])

(defmethod routes/panels :about-panel [] [about-panel])

;; main

(defn main-panel []
  (let [active-panel (<sub [::subs/active-panel])]
    (routes/panels active-panel)))
