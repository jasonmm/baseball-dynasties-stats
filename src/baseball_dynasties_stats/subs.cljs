(ns baseball-dynasties-stats.subs
  (:require
    [re-frame.core :refer [reg-sub]]))

(def <sub (comp deref re-frame.core/subscribe))

(reg-sub
  ::name
  (fn [db]
    (:name db)))

(reg-sub
  ::active-panel
  (fn [db _]
    (:active-panel db)))

(reg-sub
  ::data
  (fn [db _]
    (:data db)))

(reg-sub
  ::franchises
  (fn [db _]
    (sort-by :franchName (:franchises db))))

(reg-sub
  ::years
  (fn [db _]
    (:years db)))

(reg-sub
  ::years-input
  (fn [db _]
    (:years-input db)))

(reg-sub
  ::selected-franchise
  (fn [db _]
    (get db :franchise-id "")))

(reg-sub
  ::request-in-progress?
  (fn [db [_ request-id]]
    (get-in db [:requests request-id] false)))

(reg-sub
  ::franchise-runs
  (fn [db _]
    {:R  (get-in db [:franchise-runs :R])
     :RA (get-in db [:franchise-runs :RA])}))

(reg-sub
  ::league-mean
  (fn [db _]
    (get-in db [:league-mean :R])))

(reg-sub
  ::league-sd
  (fn [db _]
    {:R  (.toFixed (get-in db [:league-sd :R] 0) 2)
     :RA (.toFixed (get-in db [:league-sd :RA] 0) 2)}))

(reg-sub
  ::runs-scored-sd-score
  (fn [db _]
    (:r-sd-score db)))

(reg-sub
  ::runs-allowed-sd-score
  (fn [db _]
    (- 0 (:ra-sd-score db))))

(reg-sub
  ::runs-scored-sd-score-fixed-precision
  :<- [::runs-scored-sd-score]
  (fn [r-sd-score]
    (when r-sd-score
      (.toFixed r-sd-score 2))))

(reg-sub
  ::runs-allowed-sd-score-fixed-precision
  :<- [::runs-allowed-sd-score]
  (fn [ra-sd-score]
    (when ra-sd-score
      (.toFixed ra-sd-score 2))))

(reg-sub
  ::sd-score-fixed-precision
  :<- [::runs-scored-sd-score]
  :<- [::runs-allowed-sd-score]
  (fn [[r-sd-score ra-sd-score] _]
    (when (and r-sd-score ra-sd-score)
      (.toFixed (+ r-sd-score ra-sd-score) 2))))

(reg-sub
  ::sd-score-calculation-in-progress?
  (fn [db _]
    (boolean (:sd-calculation-in-progress? db))))